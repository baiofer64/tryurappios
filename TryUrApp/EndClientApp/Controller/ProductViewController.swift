//
//  ProductViewController.swift
//  FastApp
//
//  Created by Henry Bravo on 2/27/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit
import RxSwift
import RxRealm
import RealmSwift
import Domain
import Repository

class ProductViewController: UIViewController {

    @IBOutlet weak var productCollectionView: UICollectionView!
    
    var products: Products?
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let getAllProductsInteractor = GetAllProductsInteractorImpl()

        getAllProductsInteractor.execute(success: { (products) in
            self.products = products
            self.productCollectionView.delegate = self
            self.productCollectionView.dataSource = self
        }) { (error) in
            print("Error: ", error.localizedDescription)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

