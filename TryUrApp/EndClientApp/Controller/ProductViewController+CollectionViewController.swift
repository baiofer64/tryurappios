//
//  ProductViewController+CollectionViewController.swift
//  FastApp
//
//  Created by Fernando Jarilla on 6/3/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit
import Domain

extension ProductViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products?.count() ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ProductCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
        let product: Product = (products?.get(index: indexPath.row))!
        cell.refresh(product: product)
        return cell
    }
}
