//
//  BlogViewController.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 8/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit
import RxSwift
import RxRealm
import RealmSwift
import Domain
import Repository

class BlogViewController: UIViewController {
    
    
    @IBOutlet weak var blogCollectionView: UICollectionView!
    
    var posts: Posts?
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let getAllPostsInteractor = GetAllPostsInteractorImpl()
        
        getAllPostsInteractor.execute(success: { (posts) in
            self.posts = posts
            self.blogCollectionView.delegate = self
            self.blogCollectionView.dataSource = self
        }) { (error) in
            print("Error: ", error.localizedDescription)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
