import UIKit

class AddBusinessViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
   // BUSINESS prueba
    let url: String = "https://www.url.business9.com"
    var firstBusiness = BusinessUI(url: "https://www.url.business9.com", userEmail: "y1z1@gmail.com", idBusinessType: 1, idCmsType: 1)
    
    // USER
    var user  = UserDefaults.standard.object(forKey: "USER") as? UserUI
    
    
    let pickerData = [String](arrayLiteral: "Wordpress", "Magento")
   var business: Business?
    
    @IBOutlet weak var cms: UITextField!
    @IBOutlet weak var businessNameField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Init
        cms.text = "Wordpress"
        
        //delegate
         let thePicker = UIPickerView()
        
        thePicker.delegate = self
        cms.inputView = thePicker
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Picker View Delegated
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        cms.text = pickerData[row]
        self.view.endEditing(true)
    }
    
    
    // MARK - IBActions
    
    @IBAction func doneButtom(_ sender: UIBarButtonItem) {
        
            let businessName = businessNameField?.text ?? ""
            business = Business(businessName: businessName)
            //TODO: Enganchar resto de los campos
        
        //!! Guardamos negocio operacion asincrona
        //spinner
        let sv = UIViewController.displaySpinner(onView: self.view)
        
        //Time out
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
            UIViewController.removeSpinner(spinner: sv)
            self.performSegue(withIdentifier: "saveBusiness", sender: self)
        }
        
        
    //GENERALES
    func mostrarAlerta(title: String, message: String) {
        let alertaGuia = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let aceptar = UIAlertAction(title: "Aceptar", style: .default) { (action) in
        }
        alertaGuia.addAction(aceptar)
        present(alertaGuia, animated: true, completion: nil)
    }
    
 }
}
