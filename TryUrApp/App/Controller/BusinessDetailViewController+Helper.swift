//
//  BusinessDetailViewController+Helper.swift
//  TryUrApp
//
//  Created by Pedro Sánchez Castro on 25/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

extension BusinessDetailViewController {
    
    // MARK: - Helper Methods. Pasas a una extensioón
    
     func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChildViewController(viewController)
        
        
        // Add Child View as Subview
        view.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
    
     func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParentViewController: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParentViewController()
    }
    
     func loadViewControllers(identifier: String) -> UIViewController {
        //Load storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instanciate VC
        let viewController = storyboard.instantiateViewController(withIdentifier: identifier)
        
        // Add VC as a child VC
        self.add(asChildViewController: viewController)
        return viewController
    }
}
