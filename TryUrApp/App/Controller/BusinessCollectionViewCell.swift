//
//  BusinessCollectionViewCell.swift
//  TryUrApp
//
//  Created by Pedro Sánchez Castro on 21/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import UIKit

class BusinessCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var businessName: UILabel!
    
    @IBOutlet weak var url: UILabel!
}
