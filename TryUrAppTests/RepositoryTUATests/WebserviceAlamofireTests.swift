//
//  WebserviceAlamofireTests.swift
//  TryUrAppTests
//
//  Created by Fernando Jarilla on 7/5/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import XCTest
@testable import TryUrApp

class WebserviceAlamofireTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func test_webserviceAlamofire_works_properly() {
        let endpoint = "users/all"
        let param: [String: String] = [:]
        let request = endpointRequest(adding: param, and: endpoint)
        let url = request.url!
        XCTAssertTrue("\(url)" == "https://api.tryurapp.com/api/users/all?")
    }
}
