//
//  ModelTUATests.swift
//  TryUrAppTests
//
//  Created by Fernando Jarilla on 7/5/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import XCTest
@testable import TryUrApp

class ModelTUATests: XCTestCase {
    
    var userTUA: UserTUA!
    var userTUA2: UserTUA!
    var usersTUA: UsersTUA!
    var businessTUA: BusinessTUA!
    var businessTUA2: BusinessTUA!
    var responseTUA: ResponseTUA!
    var responseUsersTUA: ResponseUsersTUA!
    var responseBusinessTUA: ResponseBusinessTUA!
    var responseBusinessesTUA: ResponseBusinessesTUA!
    
    override func setUp() {
        super.setUp()
        //USERS
        //Creamos un userTUA
        userTUA = UserTUA(email: "usuarioTest@gmail.com")
        userTUA.name = "Fernando Test"
        userTUA.password = "Jarilla1"
        
        //Creamos un conjunto de 3 userTUA
        userTUA2 = UserTUA(email: "usuarioTest2@gmail.com")
        userTUA2.name = "Usuario 2"
        userTUA2.password = "Jarilla1"
        
        var userTUA3 = UserTUA(email: "usuarioTest3@gmail.com")
        userTUA3.name = "Usuario 3"
        userTUA3.password = "Jarilla1"
        
        usersTUA = UsersTUA()
        usersTUA.add(user: userTUA)
        usersTUA.add(user: userTUA2)
        usersTUA.add(user: userTUA3)
        
        //BUSINESS
        //Creamos un businessTUA
        businessTUA = BusinessTUA(url: "https://www.businessTest.com", userEmail: "businessTest@gmail.com", idBusinessType: 1, idCmsType: 1)
        
        //Creamos un segundo businessTUA
        businessTUA2 = BusinessTUA(url: "https://www.businessTest2.com", userEmail: "businessTest2@gmail.com", idBusinessType: 1, idCmsType: 1)
        
        //RESPONSES
        //Creamos una ResponseTUA
        responseTUA = ResponseTUA(status: 200, resultDescription: "Elemento guardado", result: nil)
        
        //Creamos una responseUsersTUA
        responseUsersTUA = ResponseUsersTUA(status: 200, resultDescription: "Todos los usuarios", result: [userTUA, userTUA3])
        
        //Creamos una ResponseBusinessTUA
        responseBusinessTUA = ResponseBusinessTUA(status: 200, resultDescription: "Business guardado", result: nil)
        
        //Creamos una responseBusinessesTUA
        responseBusinessesTUA = ResponseBusinessesTUA(status: 200, resultDescription: "Todos los business", result: [businessTUA, businessTUA2])
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //TESTS USERS
    func testGiven_a_userTUA_When_we_change_the_token_Then_the_token_changes() {
        userTUA.token = "Este es el nuevo token"
        XCTAssertTrue(userTUA.token == "Este es el nuevo token")
    }
    
    func testGiven_three_usersTUA_When_we_test_the_quantity_Then_the_quantity_is_three() {
        let total = usersTUA.count()
        XCTAssertTrue(total == 3)
    }
    
    func testGiven_three_usersTUA_When_we_get_element_2_Then_the_userTUA_name_is_Usuario_2() {
        let user = usersTUA.get(index: 1)
        XCTAssertTrue(user.name == "Usuario 2")
    }
    
    
    //TESTS BUSINESS
    func testGiven_a_businessTUA_When_we_change_the_logoDataName_Then_the_logoDataName_changes() {
        businessTUA.logoDataName = "logoDataTest"
        XCTAssertTrue(businessTUA.logoDataName == "logoDataTest")
    }
    
    //TESTS RESPONSES
    func testGiven_a_responseTUA_When_we_change_the_result_Then_the_result_changes() {
        responseTUA.result = userTUA
        XCTAssertTrue(responseTUA.result?.name == "Fernando Test")
    }
    
    func testGiven_a_responseUsersTUA_When_we_change_the_result_Then_the_result_changes() {
        responseUsersTUA.result?.append(userTUA2)
        XCTAssertTrue(responseUsersTUA.result?[2].name == "Usuario 2")
    }
    
    func testGiven_a_responseBusinessTUA_When_we_change_the_result_Then_the_result_changes() {
        responseBusinessTUA.result = businessTUA
        XCTAssertTrue(responseBusinessTUA.result?.userEmail == "businessTest@gmail.com")
    }
    
    func testGiven_a_responseBusinessesTUA_When_we_change_the_result_Then_the_result_changes() {
        responseBusinessesTUA.result?.append(businessTUA2)
        XCTAssertTrue(responseBusinessesTUA.result?[1].userEmail == "businessTest2@gmail.com")
    }

    
    
    
    
    
    
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
