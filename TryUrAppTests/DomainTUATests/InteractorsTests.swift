//
//  InteractorsTests.swift
//  TryUrAppTests
//
//  Created by Fernando Jarilla on 9/5/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import XCTest
@testable import TryUrApp

class InteractorsTests: XCTestCase {
    
    var user: UserUI!
    var business: BusinessUI!
    
    override func setUp() {
        super.setUp()
        
        //Creamos un userUI
        user = UserUI(email: "usuarioTest@gmail.com")
        user.name = "Fernando Test"
        user.password = "Jarilla1"
        
        //Creamos un businessUI
        business = BusinessUI(url: "https://www.businessTest.com",
                                  userEmail: "businessTest@gmail.com",
                                  idBusinessType: 1,
                                  idCmsType: 1)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func test_get_user_works_properly() {
        let ex = expectation(description: "Expecting a getUser response")
        
        let getUserInteractor = GetUserInteractorImpl()
        getUserInteractor.execute(user: user, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }

        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_get_all_users_works_properly() {
        let ex = expectation(description: "Expecting a getAllUsers response")
        
        let getAllUsersInteractor = GetAllUsersInteractorImpl()
        getAllUsersInteractor.execute(success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }

        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_auth_user_works_properly() {
        let ex = expectation(description: "Expecting a authUser response")
        
        let authUserInteractor = AuthenticateUserInteractorImpl()
        authUserInteractor.execute(user: user, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }

        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_add_user_works_properly() {
        let ex = expectation(description: "Expecting a addUser response")
        
        let addUserInteractor = AddUserInteractorImpl()
        addUserInteractor.execute(user: user, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_update_user_works_properly() {
        let ex = expectation(description: "Expecting a updateUser response")
        
        let updateUserInteractor = UpdateUserInteractorImpl()
        user.name = "Fernando updated"
        updateUserInteractor.execute(user: user, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_delete_user_works_properly() {
        let ex = expectation(description: "Expecting a deleteUser response")
        
        let deleteUserInteractor = DeleteUserInteractorImpl()
        deleteUserInteractor.execute(user: user, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }

        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_create_business_works_properly() {
        let ex = expectation(description: "Expecting a createBusiness response")
        
        let createBusinessInteractor = CreateBusinessInteractorImpl()
        createBusinessInteractor.execute(business: business, user: user, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
     
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_saveSettings_business_works_properly() {
        let ex = expectation(description: "Expecting a saveSettingsBusiness response")
        
        let saveBusinessSettingsInteractor = SaveBusinessSettingsInteractorImpl()
        saveBusinessSettingsInteractor.execute(business: business, user: user, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_get_business_works_properly() {
        let ex = expectation(description: "Expecting a getBusiness response")
        
        let getBusinessInteractor = GetBusinessInteractorImpl()
        getBusinessInteractor.execute(business: business, user: user, success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
     
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_get_all_businesses_works_properly() {
        let ex = expectation(description: "Expecting a getAllBusinesses response")
        
        let getAllBusinessesInteractor = GetAllBusinessesInteractorImpl()
        getAllBusinessesInteractor.execute(success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
       
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    func test_upload_image_works_properly() {
        let ex = expectation(description: "Expecting a uploadImage response")
       
        let uploadImageInteractor = UploadImageInteractorImpl()
        uploadImageInteractor.execute(business: business, user: user, imageType: "logo", image: "logoTUA", success: { (response) in
            XCTAssertNotNil(response)
            ex.fulfill()
        }) { (error) in
            print("ERROR: \(error)")
            XCTAssertNil(error)
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
}
