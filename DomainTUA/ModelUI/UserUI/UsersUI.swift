//
//  UsersTUA.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 9/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public struct UsersUI: Agregate {
    
    public typealias AgregateType = UserUI
    
    private var _usersUI: [UserUI]?
    
    public init() {
        self._usersUI = []
    }
    
    public func count() -> Int {
        return (_usersUI?.count)!
    }
    
    public func getAll() -> [UserUI] {
        return _usersUI!
    }
    
    public func get(index: Int) -> UserUI {
        return _usersUI![index]
    }
    
    public mutating func add(item: UserUI) {
        _usersUI?.append(item)
    }
}

extension UsersUI {
    func toString() -> [String] {
        var result = [String]()
        _usersUI?.forEach({ (user) in
            result.append(user.email)
        })
        return result
    }
}
