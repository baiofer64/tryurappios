//
//  BusinessUI.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 20/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public struct BusinessUI {
    //public var token: String = ""
    public var userEmail: String = ""
    public var url: String = ""
    public var idBusinessType: Int64 = 0
    public var idCmsType: Int64 = 0
    public var idLoadingType: Int64 = 0
    public var idAppIcon: Int64 = 0
    public var firstColorIdentifier: Int64 = 0
    public var secondColorIdentifier: Int64 = 0
    public var thirdColorIdentifier: Int64 = 0
    public var idFont: Int64 = 0
    public var isValidInfo: Bool? = true
    public var logoDataName: String? = ""
    public var welcomeImageDataName: String? = ""
    public var buildApp: String? = ""
    public var businessName: String? = ""
    
    public init(url: String, userEmail: String, idBusinessType: Int64, idCmsType: Int64) {
        self.url = url
        self.userEmail = userEmail
        self.idBusinessType = idBusinessType
        self.idCmsType = idCmsType
    }
}

extension BusinessUI {
    public func toString() -> String {
        return "\(userEmail) - \(url)"
    }
}
