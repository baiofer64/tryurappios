//
//  BusinessesUI.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 25/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public struct BusinessesUI: Agregate {
    
    public typealias AgregateType = BusinessUI
    
    private var _businessesUI: [BusinessUI]?
    
    public init() {
        self._businessesUI = []
    }
    
    public func count() -> Int {
        return (_businessesUI?.count)!
    }
    
    public func getAll() -> [BusinessUI] {
        return _businessesUI!
    }
    
    public func get(index: Int) -> BusinessUI {
        return _businessesUI![index]
    }
    
    public mutating func add(item: BusinessUI) {
        _businessesUI?.append(item)
    }
}

extension BusinessesUI {
    func toString() -> [String] {
        var result = [String]()
        _businessesUI?.forEach({ (business) in
            result.append(business.url)
        })
        return result
    }
}
