//
//  ResponseBusinessUIMapper.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 25/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public func mapResponse(from responseBusinessUI: ResponseBusinessUI) -> ResponseBusinessTUA {
    var response = ResponseBusinessTUA()
    
    response.status = responseBusinessUI.status
    response.resultDescription = responseBusinessUI.resultDescription
    response.result = mapBusiness(from: responseBusinessUI.result!)
    
    return response
}

public func mapResponse(from responseBusinessTUA: ResponseBusinessTUA) -> ResponseBusinessUI {
    var response = ResponseBusinessUI(status: 0)
    
    response.status = responseBusinessTUA.status
    response.resultDescription = responseBusinessTUA.resultDescription
    response.result = mapBusiness(from: responseBusinessTUA.result!)
    
    return response
}
