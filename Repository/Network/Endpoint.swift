//
//  Endpoint.swift
//  Repository
//
//  Created by Henry Bravo on 3/20/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public enum Endpoint {
    case configuration
    case allProducts
    case product(id: Int64)
    case allPosts
    case post(id: Int64)
}

public extension Endpoint {
    func request(with baseURL: URL, adding parameters: [String: String]) -> URLRequest {
        let url = baseURL.appendingPathComponent(path)
        
        var newParameters = self.parameters
        parameters.forEach { newParameters.updateValue($1, forKey: $0) }
        
        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)!
        components.queryItems = newParameters.map(URLQueryItem.init)
        
        var request = URLRequest(url: components.url!)
        request.httpMethod = method.rawValue
        
        return request
    }
}

private enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

private extension Endpoint {
    var method: HTTPMethod {
        switch self {
        case .configuration, .allProducts, .product, .allPosts, .post:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .configuration:
            return "configuration"
        case .allProducts:
            return "wc/v2/products"
        case .product(let id):
            return "wc/v2/products/\(id)"
        case .allPosts:
            return "wp/v2/posts"
        case .post(let id):
            return "wp/v2/post/\(id)"
        }
    }
    var parameters: [String: String] {
        switch self {
        case .configuration:
            return [:]
        case .allProducts:
            return [:]
        case .product:
            return [:]
        case .allPosts:
            return [:]
        case .post:
            return [:]
        }
    }
}
