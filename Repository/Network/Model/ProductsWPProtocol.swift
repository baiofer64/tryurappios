//
//  ProductsWPProtocol.swift
//  Repository
//
//  Created by Fernando Jarilla on 25/3/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

protocol ProductsWPProtocol {
    func count() -> Int
    mutating func add(product: ProductWP)
    func get(index: Int) -> ProductWP
}
