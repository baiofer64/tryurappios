##AppClient

Dispone de los módulos Repository y Domain

###WEBSERVICE

El uso del WebService se realiza mediante la función **load()**
El formato es:

    load(_ type: T.type, from endpoint: String) -> Observable<T>

###USO

    webService
        .load([ProductWP].self, from: endpoint)
        .observeOn(MainScheduler.instance)
        .subscribe(
            onNext: { product in
            }
        )

Ya implementa la autenticación por medio de **consumer_key** y **consumer_secret**

###INTERACTORS

Se han implementado los interactors

    getAllPostsInteractorImpl()
    getAllProductsInteractorImpl()

##TryUrApp

Dispone de los módulos RepositoryTUA y DomainTUA

###WEBSERVICE

El uso del WebService se realiza mediante la función **endpointRequest()**

El formato es:

    endpointRequest(adding parameters: [String: String], and path: String) -> URLRequest

###USO

    let endpoint = "users/register"
    let token = ["token": ""]
    let request = endpointRequest(adding: token, and: endpoint)
    
    Alamofire.request(request)

La autenticación se realiza por **token** y ya está implementada en los interactors correspondientes

###INTERACTORS

Se han implementado los interactors :

Para USERS

    getAllUsersInteractorImpl(success: ResponseUsersSuccessClosure, errorClosure: ErrorClosure?)
    getUserInteractorImpl(user: UserTUA, success: ResponseSuccessClosure, errorClosure: ErrorClosure?)
    addUserInteractorImpl(user: UserTUA, success: ResponseSuccessClosure, errorClosure: ErrorClosure?)
    authenticateUserInteractorImpl(user: UserTUA, success: ResponseSuccessClosure, errorClosure: ErrorClosure?)
    deleteUserInteractorImpl(user: UserTUA, success: ResponseSuccessClosure, errorClosure: ErrorClosure?)
    updateUserInteractorImpl(user: UserTUA, success: ResponseSuccessClosure, errorClosure: ErrorClosure?)

Para BUSINESS

    createBusiness(business: BusinessTUA, user: UserTUA, success: ResponseSuccessClosure, errorClosure: ErrorClosure?)
    saveBusinessSettings(business: BusinessSettingsTUA, user: UserTUA, success: ResponseSuccessClosure, errorClosure: ErrorClosure?)

