//
//  UsersTUA.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 9/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import RealmSwift

public struct UsersTUA: UsersTUAProtocol, Decodable {
    
    private var userTUA: [UserTUA]?
    
    public init() {
        self.userTUA = []
    }
    
    public func count() -> Int {
        return (userTUA?.count)!
    }
    
    public mutating func add(user: UserTUA) {
        userTUA?.append(user)
    }
    
    public func get(index: Int) -> UserTUA {
        return (userTUA?[index])!
    }
}
