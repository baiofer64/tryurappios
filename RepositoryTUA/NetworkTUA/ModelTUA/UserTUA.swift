//
//  UserTUA.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 9/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import RealmSwift

public struct UserTUA: Decodable {
    public var email: String
    var name: String? = ""
    var password: String? = ""
    var token: String? = ""
    var _id: String? = ""
    var creationDate: String? = ""
    var deletedDate: String? = ""
    var lastModifiedDate: String? = ""
    var isActive: Bool? = true
    
    private enum CodingKeys: String, CodingKey {
        case email
        case name
        case password
        case token
        case _id
        case creationDate
        case deletedDate
        case lastModifiedDate
        case isActive
    }
    init(email: String) {
        self.email = email
    }
}

