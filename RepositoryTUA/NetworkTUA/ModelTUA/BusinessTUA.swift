//
//  BusinessTUA.swift
//  TryUrApp
//
//  Created by Fernando Jarilla on 20/4/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public struct BusinessTUA: Decodable {
    //var token: String?
    var userEmail: String?
    var url: String? = ""
    var idBusinessType: Int64? = 0
    var idCmsType: Int64? = 0
    var idLoadingType: Int64? = 0
    var idAppIcon: Int64? = 0
    var firstColorIdentifier: Int64? = 0
    var secondColorIdentifier: Int64? = 0
    var thirdColorIdentifier: Int64? = 0
    var idFont: Int64? = 0
    var isValidInfo: Bool? = true
    var logoDataName: String? = ""
    var welcomeImageDataName: String? = ""
    var buildApp: String? = ""
    var businessName: String? = ""
    
    private enum CodingKeys: String, CodingKey {
        //case token
        case userEmail
        case url
        case idBusinessType
        case idCmsType
        case idLoadingType
        case idAppIcon
        case secondColorIdentifier
        case thirdColorIdentifier
        case idFont
        case isValidInfo
        case logoDataName
        case welcomeImageDataName
        case buildApp
        case businessName
    }

    public init(url: String, userEmail: String, idBusinessType: Int64, idCmsType: Int64) {
        self.url = url
        self.userEmail = userEmail
        self.idBusinessType = idBusinessType
        self.idCmsType = idCmsType
    }
}
