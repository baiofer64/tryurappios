//
//  GetAllItemsInteractorImpl.swift
//  Domain
//
//  Created by Henry Bravo on 4/1/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation
import Repository

public class GetAllProductsInteractorImpl: GetAllItems {
    
    public typealias ItemsType = Products
    
    public init() {}
    
    public func execute(success: @escaping (Products) -> Void, onError: @escaping (Error) -> Void) {
        let repository: Repository = RepositoryImpl()
        
        repository.getAllProducts(success: { (items) in
            var products = Products()
            
            items.forEach({ (item) in
                let product = mapProduct(from: item)
                products.add(item: product)
            })
            success(products)
            
        }) { (error) in
                onError(error)
        }
    }
}
