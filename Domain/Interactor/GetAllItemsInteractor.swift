//
//  GetAllProductsInteractor.swift
//  Domain
//
//  Created by Henry Bravo on 4/1/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

import Repository

public protocol GetAllItems {
    associatedtype ItemsType
    
    func execute(success: @escaping ItemsSuccessClosure<ItemsType>, onError: @escaping ErrorClosure)
}


