//
//  Product.swift
//  Domain
//
//  Created by Henry Bravo on 4/1/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public struct Product {
    public var identifier: Int64 = 0
    public var title: String = ""
    public var image: String? = nil
    public var salePrice: Double? = nil
    public var stock: Int = 0
    public var descriptionProduct: String = ""
    public var height: String = ""
    public var width: String = ""
    public var length: String = ""
    public var taxClass: String = ""
    public var shortDescription: String = ""
    
    public init(title: String) {
        self.title = title
    }
}
