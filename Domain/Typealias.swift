//
//  Typealias.swift
//  Domain
//
//  Created by Henry Bravo on 4/1/18.
//  Copyright © 2018 Henry Bravo. All rights reserved.
//

import Foundation

public typealias ItemsSuccessClosure<T> = (T) -> Void
